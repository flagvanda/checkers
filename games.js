document.addEventListener("DOMContentLoaded", () => {
	
	const pieces = document.querySelectorAll("#board .piece");

	const squares = document.querySelectorAll("#board .black");
	
	let pieceSelected = false;
	const currentPlayer = "black";

	const uncheckPiece = function() {
		this.classList.remove("selected");
		pieceSelected = false;
		this.removeEventListener("click", uncheckPiece);
		
	}
	
	
	
	for(let i = 0; i < pieces.length ; i++) {
		
		pieces[i].addEventListener("click", () => {
			
			if(!pieceSelected && pieces[i].classList.contains("piece-" + currentPlayer)) {
				pieces[i].classList.add("selected");
				pieceSelected = true;
				
				if(squares[16].children.length === 0) {
					squares[16].innerHTML = '<div class="piece piece-' +currentPlayer+ '"></div>';
					pieces[i].remove();
				}
				
				
				
				//document.querySelector("#board .selected").addEventListener("click", uncheckPiece);
			}
			
			
		});
	}

	
	
});